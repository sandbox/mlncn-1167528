<?php

/**
 * Formats a link in a category's TOC display, and appends an assigned node
 * count if applicable.
 *
 * @ingroup themeable
 */
function zing_category_display_toc_link($category, $orig_node) {
if (function_exists('taxonomy_image_display')) $taxonomy_image = taxonomy_image_display($category->cid, 'class="taxonomy_image"');
else $taxonomy_image = FALSE;
  return '<span class="left"><big>' . l($category->title, 'node/'. $category->cid) . ($orig_node->toc_nodecount && $category->cnid ? '</big> <span class="category-toc-node-count">('. category_category_count_nodes($category->cid). ')</span></span>' : '') . ($taxonomy_image ? $taxonomy_image : '');
}

/* how to override store_user function?
function theme_store_user() {
  return "";
}
*/

function zing_regions() {
    return array(
        // We'll want to return the default regions
        'left' => t('left sidebar'),
        'right' => t('right sidebar'),
        'content' => t('content'),
        'header' => t('header'),
        'footer' => t('footer'),
        // !!Here's our new regions!!
        'feature' => t('feature'),
        'login' => t('login')
    );
}

function phptemplate_upload_images_body($body, $thumbnails) {
  return theme('upload_images', $thumbnails) . $body;
}

function phptemplate_upload_images($list) {
  $output = '<div class="upload-image-images">';
  $output .= theme('item_list', $list);
  $output .= '</div>';
  return $output;
}