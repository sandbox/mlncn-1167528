<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
  <title> <?php print $head_title ?> </title>
  <meta http-equiv="Content-Style-Type" content="text/css" />
   <?php print $head ?>
   <?php print $styles ?>
</head>
<body <?php print theme("onload_attribute"); ?> >
<div id="all">
<div id="header">
<table width="100%">
<tr><td width="10%"><a href="<?php print url() ?>" title="Auction Home"><img src="<?php print base_path() . path_to_theme() ?>/images/logo.png" alt="Zing Fling Online Auction"></a></td>
<td width="40%" style="vertical-align: top">
   <?php if ($site_name) : ?>
    <h1 id="site-name"><a href="<?php print url() ?>" title="Auction Home"><?php print($site_name) ?></a></h1>
   <?php endif;?>
   <?php if ($site_slogan) : ?>
    <span id="site-slogan"> <?php print($site_slogan) ?> </span>
   <?php endif;?>
</td>
<td width="40%" align="center">

                <?php /* bmm added per nick lewis */ if ($feature) { ?>
<div id="feature">
<?php print $feature; ?>
</div>
<?php } else { ?>

<a href="/auction2010/about"><img align="center" src="<?php print base_path() . path_to_theme() ?>/images/sponsorlogos2010.gif" alt="Sponsors animated banner"></a>

<?php } /* end else (no feature block) content */ ?>

</td>
<td width="10%">
    <a href="<?php print url() ?>node/1" title="Welcome Page" id="right-logo"><img src="<?php print base_path() . path_to_theme() ?>/images/ATAC.gif" alt="Amazing Things Arts Center Logo" /></a>
</td></tr></table>
</div>

  <div id="top-nav">
   <?php if (is_array($primary_links) && count($primary_links) != 0) : ?>
    <ul id="primary"><?php foreach ($primary_links as $link): ?><li><?php print $link?></li><?php endforeach; ?></ul>
   <?php endif; ?>
   <?php if (is_array($secondary_links) && count($secondary_links) != 0) : ?>
    <ul id="secondary">
     <?php foreach ($secondary_links as $link): ?>
      <li> <?php print $link?> </li>
     <?php endforeach; ?>
    </ul>
   <?php endif; ?>
  </div>

<table id="content">
  <tr>
     <?php if ($sidebar_left != ""): ?>
      <td class="sidebar" id="sidebar-left">
         <?php print $sidebar_left ?>
      </td>
     <?php endif; ?>
        <td class="main-content" id="content-<?php print $layout ?>">
         <?php if ($title != ""): ?>
          <h2 class="content-title"><?php if ($node->type == 'category-cat') {
            // need a function-exists or something here
            $taxonomy_image = taxonomy_image_display($node->nid, 'class="taxonomy_image"');
            if ($taxonomy_image) {
                print $taxonomy_image;
                print "&nbsp; ";
            }
         } ?>
         <?php print $title ?></h2>
         <?php endif; ?>
         <?php if ($tabs != ""): ?>
           <?php print $tabs ?>
         <?php endif; ?>
         <?php if ($mission != ""): ?>
          <p id="mission"> <?php print $mission ?> </p>
         <?php endif; ?>
         <?php if ($help != ""): ?>
          <p id="help"> <?php print $help ?> </p>
         <?php endif; ?>
         <?php if ($messages != ""): ?>
          <div id="message"> <?php print $messages ?> </div>
         <?php endif; ?>
         
         <?php /* block for prominent loginbmm added per nick lewis */
         if ($login) { ?>
          <div id="login">
          <?php print $login; ?>
          </div>
         <?php } /* else no login block */ ?>
         
        <!-- start main content -->
         <?php print($content) ?>
        <!-- end main content -->
        </td><!-- mainContent -->
     <?php if ($sidebar_right != ""): ?>
    <td class="sidebar" id="sidebar-right">
         <?php print $sidebar_right ?>
    </td>
     <?php endif; ?>
  </tr>
</table>
<?php if ($breadcrumb != ""): ?>
   <?php print $breadcrumb ?>
<?php endif; ?>
  <?php print $closure;?>
  </div>
  <div id="footer">
    <?php if ($footer_message) : ?>
      <p> <?php print $footer_message;?> </p>
    <?php endif; ?>
  </div><!-- footer -->
  </body>
</html>

