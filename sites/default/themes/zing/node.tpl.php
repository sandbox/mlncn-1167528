
<div class="node<?php print ($sticky && $page==0) ? " sticky" : ""; 
                      print ($status) ? " published" : " unpublished"; ?>">
   <?php if ($page == 0):
   $anchor_id = ' name="node-' . $nid . '" id="node-' . $nid . '" ';
   ?>
     <h2><a <?php print $anchor_id ?> href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
   <?php endif; ?>
  <div class="content">
     <?php print $picture ?>
     <?php print $content ?>
  </div>
  <?php if ($picture): ?>
    <br class='clear' />
  <?php endif; ?>
  <div class="info">
    <?php print $submitted ?>
    <?php if($terms): ?>
      <span class="terms">category: <?php print $terms ?> </span>
    <?php endif; /* deleted $links */ ?>
  </div>
</div>
