upload_image.module takes uploaded images and copies them to image nodes.

The module depends on both upload.module and image.module.

To Do
-----
Improve CSS

Author
------
Gerhard Killesreiter

B�r Kessels had made a now abandoned module with the same name and a similar
purpose. They share no code.

LICENCE
-------

GNU General Public Licence

$Id: README.txt,v 1.2 2006/01/03 19:24:34 killes Exp $
